# ¡Hola!

Soy Miguel, ¡encantado!

Si vienes porque hago comedia, puedes saber más de mí [aquí](page/about/comedy), 
suscribirte a mi newsletter [aquí](https://news.miguescri.com), 
o conocer mi productora [aquí](https://somardas.co).

---

# Hi!

I'm Mike, nice to meet you!

If you're here because I'm an engineer, you can learn about my work [here](page/about/tech), 
or read my tech blog [here](post).