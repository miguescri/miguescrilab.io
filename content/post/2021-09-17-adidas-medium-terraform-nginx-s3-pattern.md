---
title: "adidas Engineering Blog post: Plugggable NGINX configuration  with S3 and Terraform"
date: 2021-09-17
tags: ["cloud", "terraform"]
---

I've published a post in the [adidas Engineering Blog](https://medium.com/adidoescode) about a pattern to configure
a NGINX proxy server in front of multiple apps in AWS using Terraform. The tricky part: keeping each app in its 
own separate project with different state files.

[Check it out here.](https://medium.com/adidoescode/pluggable-nginx-configuration-with-s3-and-terraform-c0510d79679a)
