---
title: "Open source and business"
date: 2020-01-17
tags: ["open-source", "technology", "business"]
---

This is an article I wrote (in spanish) a few days ago for the blog of Pulsar, an organization I'm part of that promotes the se of open source software.

The original can be found [here](https://pulsar.unizar.es/software-libre-y-empresas/).

---

En muchos casos se piensa del software libre como la contraposición al software ofrecido por compañías privadas. Si bien históricamente el software libre ha sido promovido por individuos y fundaciones para ofrecer alternativas abiertas a programas de código cerrado, esto ha cambiado en los últimos años.

El software libre ya no tiene que ser un peligro para las empresas, sino que puede ayudar al desarrollo comercial de las mismas. Y no hablamos únicamente de que las empresas hagan uso de software libre disponible para abaratar su procesos internos, sino de dedicar grandes cantidades de recursos a contribuir al desarrollo de proyecto existentes o nuevos.

El que un gran número de empresas hayan apostado por desarrollar software libre ha dado un importante empujón a proyectos que previamente se nutrían exclusivamente del esfuerzo voluntario de miles de desarrolladores. El principal beneficio de la colaboración empresarial es que al haber programadores cuyo jornada laboral está parcial o totalmente dedicada a contribuir a ciertos proyectos, estos tienen un ritmo de desarrollo más rápido y estable. Esto no es nada desdeñable cuando se observa la enorme cantidad de proyectos que han languidecido y muerto por falta de tiempo de los colaboradores.

¿Y por qué algunas empresas han pasado de las posiciones privativas férreas de décadas anteriores a ser abanderadas del software libre? Porque les beneficia, por supuesto. El beneficio puede categorizarse en dos tipos (tres si contamos el bienestar emocional de hacer el mundo un poco mejor, claro): beneficio económico e influencia. ¡Veamos algunos ejemplos de cada!

## Software libre como modelo económico

No nos engañemos, aunque el software libre no cueste dinero, nunca es gratis. Sea en tiempo o en disgustos, algo pagas para aprender a usar una herramienta de forma eficiente. Esto es especialmente caro si tienes un negocio y tratas de sustituir una herramienta privativa por una de software libre en tu equipo.

Algunas empresas han visto este problema y han basado su modelo de negocio en facilitar la vida de otras empresas. En su día a día ayudan a desarrollar un software libre y simultáneamente ofrecen servicios de pago para ese software: cursos, soporte técnico, alojamiento, funcionalidades extra para coordinación de equipos y reparto de tareas...

Un ejemplo de este modelo económico es [Gitlab](https://about.gitlab.com), que desarrolla el proyecto homónimo, un sistema que unifica el control de todo el ciclo de vida de un producto software. El [proyecto](https://gitlab.com/gitlab-org/gitlab) se divide en la Community Edition (Gitlab CE), bajo licencia MIT Expat, y la Enterprise Edition (Gitlab EE), una capa de pago extra sobre la CE con copyright. Gitlab CE cubre todas las necesidades de un usuario individual o una pequeña organización, mientras que Gitlab EE ofrece funcionalidades más completas de coordinación de tareas, monitorización de esfuerzos y gestión de calidad, además de soporte técnico prioritario, las cuales sólo son realmente necesarias para empresas. También, tanto para CE como EE, Gitlab (empresa) ofrece hosting cloud del servicio.

[Hashicorp](https://www.hashicorp.com) sigue una estrategia similar con su software [Terraform](https://www.hashicorp.com/products/terraform) destinado a la gestión de infraestructura para proyectos cloud. La mayor parte del [proyecto](https://github.com/hashicorp/terraform) está disponible bajo licencia Mozilla Public License 2.0, y Hashicorp oferta funcionalidades de gestión empresarial y hosting cloud.

Hay muchos más ejemplos, como [Jetbrains](https://www.jetbrains.com) con su IDE [IntelliJ](https://github.com/JetBrains/intellij-community), el cual ofrece más facilidades en [la versión de pago](https://www.jetbrains.com/idea/), o [MongoDB](https://github.com/mongodb/mongo), que ofrece [hosting cloud](https://www.mongodb.com/cloud/atlas) autoconfigurado y escalable.


## Software libre como generador de influencia

El día a día de una empresa tecnológica a veces incluye enfrentarse a un problema que no ha sido resuelto previamente o cuya solución previa no es apta para el caso concreto. Esto da lugar a desarrollar internamente herramientas que pueden alcanzar altos grados de sofisticación. Una vez la herramienta ha madurado y ha probado su eficacia, la empresa puede mantenerla privada o publicarla como software libre. 

Por un lado, mantener la herramienta privada puede dar una ventaja competitiva sobre otros rivales, pero por otro, abrirla puede dar otras ventajas interesantes. Si un proyecto de software libre es el primero en solucionar de forma eficiente un problema, este puede servir de punta de lanza para investigar la forma de afrontar el problema, atrayendo desarrolladores y usuarios, y potencialmente convirtiéndose en un estándar de facto para ese ámbito.

Si se alcanza esta situación, la empresa contará con el apoyo de muchos desarrolladores de todo el mundo para perfeccionar una herramienta que se usa internamente, abaratando el coste de I+D. Además, potencialmente aparecerán nuevos proyectos de software libre relacionados y compatibles con este nuevo estándar, lo que, de nuevo, ahorra trabajo en integrar nuevas tecnologías con la infraestructura de la empresa. Y, por supuesto, el nombre de la empresa pasa a estar asociado con una tecnología útil de libre acceso, proporcionando un buen elemento de marketing.

Un caso ejemplar de esto fue la creación de [Kubernetes](https://github.com/kubernetes/kubernetes) por parte de Google. Kubernetes es una adaptación de una herramienta interna que desarrollaron durante una década para gestionar su enorme cantidad de servicios. El nivel de refinación del proyecto y toda la experiencia práctica volcado en él permitió al conjunto de la industria evolucionar de forma muy rápida, y hoy por hoy no se concibe el mundo del cloud sin Kubernetes.

Otro buen ejemplo es la liberación de [ReactJS](https://github.com/facebook/react) por parte de Facebook. React es una tecnología que Facebook desarrolló para crear de forma eficiente aplicaciones web con elementos reutilizables. Su sencillez de uso ha enamorado a muchísimos desarrolladores que a su vez no paran de contribuir nuevas funcionalidades, para beneficio de Facebook.

Y por último, un caso notable de creación masiva de software libre es Netfilx. En su proceso de crear un sistema capaz de proveer de vídeo de alta calidad de forma ininterrumpida a millones de usuarios, Netflix ha descubierto y solucionado muchos problemas completamente nuevos para la industria, y [ha liberado gran parte de sus herramientas](https://github.com/Netflix) para regocijo del resto del mundo. La visibilización del tipo y calidad de las tecnologías creadas dentro de Netflix está atrayendo a algunos de los mejores desarrolladores del mundo a querer trabajar para ellos, revirtiendo en que el equipo de ingenieros de Netflix sea cada vez más potente.


---

Ya ves que tener un negocio tecnológico no es incompatible con aportar conocimiento al mundo a través de software libre. Granito a granito creamos el futuro de la tecnología.

¡Hasta otra!





