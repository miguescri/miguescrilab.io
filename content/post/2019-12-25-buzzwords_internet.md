---
title: "Buzzwords: Internet"
date: 2019-12-25
tags: ["podcast", "buzzwords"]
---

[Listen here!](https://anchor.fm/buzzwords/episodes/Internet-e9pog3/a-a17mem7)


Internet is one of the most important concepts in human history, if not the most.

Born in sixties, it became a cultural phenomenon in the nineties with the creation of the web and the first web browsers.

Nowadays, Internet is the foundation upon which most of the infraestructure and services we need in a daily basis are built on. &nbsp;And, of course, it has changed the way we understand social interactions and information sharing.

Internet is foundamental in our lives, but, do you know what is Internet? How does Facebook's webpage appear in our laptops when we write facebook.com? How is it possible that apps talk to each other? Where is Internet?

---

**Concepts of the episode**

- [Internet](https://en.wikipedia.org/wiki/Internet)
- [Internet protocol](https://en.wikipedia.org/wiki/Internet_Protocol)
- [Router](https://en.wikipedia.org/wiki/Router_(computing))
- [DNS](https://en.wikipedia.org/wiki/Domain_Name_System")

---

**Credits**

- Intro-outro music: [Six Umbrellas - Magic](https://www.youtube.com/watch?v=Esc4i1RNYTo)
- Background sounds: [Zapslat.com](https://www.zapsplat.com/)


