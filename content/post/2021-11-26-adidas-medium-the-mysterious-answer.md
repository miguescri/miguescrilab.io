---
title: "adidas Engineering Blog post: The mysterious answer"
date: 2021-11-26
tags: ["cloud", "dns", "high-availability"]
---

I've published another post in the [adidas Engineering Blog](https://medium.com/adidoescode). This time, I talk
about a mysterious bug that haunted us for months and that turned out to be *Yet Another DNS Fuckup*.

[Check it out here.](https://medium.com/adidoescode/the-mysterious-answer-2a1af303b88c)
