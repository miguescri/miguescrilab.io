---
title: "Web technologies"
date: 2020-01-09
tags: ["web", "technology", "internet"]
---

## Intro

In 1992, a scientist working at CERN created HTTP, a protocol to access files stored in remote computers, starting the Web as we know it. Since then, the scope of the usage of the Web has broadened greatly. For each new challenge, new approaches and technologies were introduced into the ecosystem.

As a result, the Web nowadays is built on top of a very complex and heterogeneus set of technologies that developpers need to take into consideration when building or maintainning systems.

In the followings sections we will be looking into some of the available technologies, each of which represents one of the different big approaches that have been used during the Web history. The purpose of this is getting to understand their differences and when to use each.

## Technologies

### OpenAPI

OpenAPI, formerly Swagger, is not a technology by itself, but a specification for the definition of REST APIs over HTTP/1.1. OpenAPI allows to detail all of the following HTTP elements and more:

- URI endpoints.
- HTTP verbs.
- Key/value URI params, body payloads, response codes and payloads, headers and cookies. 
- Negotiable representations, such as JSON, XML or other file formats.
- Custom complex types in JSON and XML.
- Requests' permissions required.
- Authentication methods.

Once an API is defined some of the tools of the Swagger/OpenAPI ecosystem, such as swagger-codegen, can be used to generate server stubs and client SDKs for many languages and frameworks.

#### Pros

It is easy to use and integrate:

- The granularity of the definitions and the simple interfaces of the generated code allow to make the most out of HTTP 1.1 without having to manually implement all the negotiations required.
- The language agnostic contract-first approach helps to create loosely coupled systems easier to maintain.
- HTTP/1.1 is still the only HTTP version accepted by older software.
- While it is useful to use the generated client SDK, it is not needed to craft a valid HTTP message.
- If designed with a pure REST approach, APIs are intuitive to understand and use.

#### Cons

It is slow and sends redundant, non-compressed data:

- HTTP/1.1 creates a unique half-duplex TCP connection for each request.
- If designed with a pure REST approach, complex operations require a great number of requests.
- HTTP/1.1 doesn't compress headers, which may be a big overhead for small size requests.
- Information is usually formated as uncompressed JSON, which increases heavily the size over serialized binary formats.

#### Example

An inventory managemente application where client makes CRUD requests to the server about items and colections of items identified by URIs.


### gRPC

gRPC is a RPC system originally developped by Google that uses Protocol Buffers as its definition language. 

RPC stands for Remote Procedure Call and represents the idea of defining functions and call them in a machine, while they are actually executed in a remote one. gRPC is a modern approach to this, but in the early stages of the Web the biggest contestor in this technique was SOAP, which relied completely on HTTP to work.

gRPC allows to define:

- Custom complex serializable data types.
- Functions and methods with typed parameters and return values.
- Functions and methods set to be syncronous or asyncronous.
- Parameters and return values set to be streams instead of individual messages.

Once the definition is done, server stubs and client SDKs are generated and used transparently as local functions.

![gRPC component diagram](https://grpc.io/img/landing-2.svg)

gRPC uses HTTP/2 only as its transport layer and implements its own on-wire messaging format on top of it. For browsers, gRPC-web communicates from the client over HTTP/1.1 via a proxy (Envoy or Nginx), which relais the requests over HTTP/2.

![gRPC-web diagram](https://cdn-media-1.freecodecamp.org/images/1*PJce89y7GZdBYsiHzmmUow.jpeg)

Also, grpc-gateway generates a HTTP service from the Protocol Buffers definition that interfaces with the gRPC server.

![gRPC gateway diagram](https://grpc.io/img/grpc-rest-gateway.png)

#### Pros

Very efficient and makes it easy to build microservice systems:

- The language agnostic contract-first approach helps to create loosely coupled systems easier to maintain.
- HTTP/2 full-duplex multi-request connections, HTTP/2 compression and data serialization result into **very** fast connections (in official C++ benchmarks, 150 microseconds for ping-pong example).
- Solutions to integrate with REST and browsers allow to maintain only one interface code source.

#### Cons

Difficult usage for third parties:

- HTTP/2 adoption started in 2015 and previous software may not support it.
- HTTP/1.1 gateway adds new complexity in the system infraestructure and reduces performance.
- It forces clients to use the SDK to connect to the gRPC interface.

#### Example

A translation service that accepts text in a language and returns it in another one. This could be implemented so that the user streams text in the original language and receives in real time a stream with the translation.


### Push API

Push API is a system that allows a program running in a browser to subscribe itself to a remote service, that will send asyncronous events over time. These events will reach the program even if the tab/agent is no longer active, as far as the browser is still running.

There needs to be a push server that interfaces between the browser and the backend server of the app (in the case of Firefox, for example, the push server is managed by the Mozilla Foundation). 

Also, browsers need to run background service workers which keep an active connection to receive and forward the events from the push service. The original application can choose how the worker will handle this, for example showing a notification to the user or modifying the internal state.

![W3C sequence diagram for Push API](https://w3c.github.io/push-api/sequence_diagram.png)

#### Pros

Simplifies and improves the efficiency of streaming live updates to client systems:

- Avoids polling for updates reducing messages exchanged and latencies between creation and delivery of events.
- Centralizes the connection management in the browser, optimizing resources usage.
- Centralized push servers, so event creator don't need to implement big publish-subscribe systems.

#### Cons

It's usage scenarios are very restricted:

- Browser adoption started in 2016 and not all major browsers support it yet.
- Programns need to run on a browser or implement service workers.

#### Example

A trading website in which the user selects a group of companies to follow and receives pop-up browser notifications when the stock of those changes.


### WebSocket

WebSocket is a bidireccional communication protocol that allows a webapp to send and receive asyncronously messages of any size and type to a WebSocket server.

WebSocket uses the HTTP initial handshake to start a connection, but immediately after changes to its own TCP-based protocol. Once the connection is stablished, both client and server can send messages at any given time until one of them chooses to close the connection.

![Oracle sequence diagram for Websockets](https://docs.oracle.com/cd/E55956_01/doc.11123/user_guide/content/images/general/websocket_sequence.png)

The messages can be strings, blobs and byte arrays, allowing to transmit any kind of information.

#### Pros

Extremelly flexible and very efficient.

- As a transport protocol, doesn't constraint what is built on top.
- Server can communicate with client whenever it wants.
- Custom TCP protocol achieves better performance than HTTP communications.
- Adopted by major browsers since 2012.

#### Cons

Adds complexity to the system development and maintenance.

- Client and server codes are tightly coupled.
- There is no explicit code API contract.
- Load balancing is difficult due to clients being tied to specific servers.

#### Example

An online videogame, where user introduces orders to move the character and receives at the same time the new location of the other players.


## Final thoughts

As we have seen, there is not such a thing as a perfect tool, but instead it all comes down to the scenario at hand. So, when deciding which technology to use in your project, you have to ask yourself a couple of questions such as:

- Are my operations related to retrieve and modify discrete objects and collections? Or are they  more abstract?
- Do I need a bidirectional communication?
- Should updates reach a client as soon as they appear? Or cand it wait until a manual query?
- Am I the only one who will write clients that connect to the service? Or will others need a defined API/SDK?

With this questions and more you will get a better idea of what are your needs. And, of course, it may happen that your needs can be solved by combining different technologies.

Just make sure that you are a good internet citizens and follow best practices!

## References

### HTTP

- IETF, *RFC 7230: Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing*, [https://tools.ietf.org/html/rfc7230](https://tools.ietf.org/html/rfc7230)
- IETF, *RFC 7231: Hypertext Transfer Protocol (HTTP/1.1): Semantics and content*, [https://tools.ietf.org/html/rfc7231](https://tools.ietf.org/html/rfc7231)
- IETF, *RFC 7540: Hypertext Transfer Protocol Version 2 (HTTP/2)*, [https://tools.ietf.org/html/rfc7540](https://tools.ietf.org/html/rfc7540)
- IETF, *RFC 7541: HPACK: Header Compression for HTTP/2*, [https://tools.ietf.org/html/rfc7541](https://tools.ietf.org/html/rfc7541)
- CanIUse, HTTP/2 browser adoption chart, [https://caniuse.com/#search=http%2F2](https://caniuse.com/#search=http%2F2)

### gRPC

- CNCF, gRPC official site, [https://grpc.io/](https://grpc.io/)
- Google, Protocol Buffers official site, [https://developers.google.com/protocol-buffers/](https://developers.google.com/protocol-buffers/)
- CNCF, *gRPC over HTTP/2*, [https://github.com/grpc/grpc/blob/master/doc/PROTOCOL-HTTP2.md](https://github.com/grpc/grpc/blob/master/doc/PROTOCOL-HTTP2.md)
- CNCF, gRPC-web for browser clients, [https://github.com/grpc/grpc-web](https://github.com/grpc/grpc-web)
- CNCF, grpc-gateway for REST interaction, [https://github.com/grpc-ecosystem/grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway)
- CNCF, gRPC benchmarks, [https://grpc.github.io/docs/guides/benchmarking.html](https://grpc.github.io/docs/guides/benchmarking.html)
- David Cao, *gRPC mobile benchmarks*, [https://github.com/david-cao/gRPCBenchmarks](https://github.com/david-cao/gRPCBenchmarks)
- freeCodeCamp, gRPC-web diagram, [https://cdn-media-1.freecodecamp.org/images/1*PJce89y7GZdBYsiHzmmUow.jpeg](https://cdn-media-1.freecodecamp.org/images/1*PJce89y7GZdBYsiHzmmUow.jpeg)

### OpenAPI

- OpenAPI Initiative, OpenAPI specification version 3.0.2, [https://spec.openapis.org/oas/v3.0.2](https://spec.openapis.org/oas/v3.0.2)
- Swagger, Swagger and OpenAPI code generator, [https://swagger.io/tools/swagger-codegen/](https://swagger.io/tools/swagger-codegen/)
- Red Hat, *Comparing OpenAPI with gRPC*, [https://servicesblog.redhat.com/2019/01/31/comparing-openapi-with-grpc/](https://servicesblog.redhat.com/2019/01/31/comparing-openapi-with-grpc/)
- Google, *Resource oriented design*, [https://cloud.google.com/apis/design/resources](https://cloud.google.com/apis/design/resources)

### Push API

- W3C, Push API specification, [https://w3c.github.io/push-api](https://w3c.github.io/push-api)
- IETF, *RFC 8030: Generic Event Delivery Using HTTP Push* [https://tools.ietf.org/html/rfc8030](https://tools.ietf.org/html/rfc8030)
- CanIUse, Push API browser adoption chart, [https://caniuse.com/#feat=push-api](https://caniuse.com/#feat=push-api)
- Mozilla Foundation, *Web Push notifications in Firefox*, [https://support.mozilla.org/en-US/kb/push-notifications-firefox](https://support.mozilla.org/en-US/kb/push-notifications-firefox)
- W3C, Sequence diagram for Push API, [https://w3c.github.io/push-api/sequence_diagram.png](https://w3c.github.io/push-api/sequence_diagram.png)

### WebSocket

- IETF, *RFC 6455: The WebSocket Protocol*, [https://tools.ietf.org/html/rfc6455](https://tools.ietf.org/html/rfc6455)
- Mozilla Foundation, *The WebSocket API*, [https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)
- CanIUse, WebSocket browser adoption chart, [https://caniuse.com/#feat=websockets](https://caniuse.com/#feat=websockets)
- Oracle, Websocket sequence diagram, [https://docs.oracle.com/cd/E55956_01/doc.11123/user_guide/content/images/general/websocket_sequence.png](https://docs.oracle.com/cd/E55956_01/doc.11123/user_guide/content/images/general/websocket_sequence.png)


