---
title: "New podcast: Buzzwords"
date: 2019-12-22
tags: ["podcast", "buzzwords"]
---

So, it happened. I started a podcast!

>Internet, operative systems, artificial intelligence, blockchain...
>
>These buzzwords are repeated over and over again in press and marketing campaigns. Probably you also use them in your conversations.
>
>But do we understand what these words represent? Or do we just accept them as magical gifts that only nerds can comprehend?
>
>In this podcast I'll explain in simple terms some of the technological concepts that shape the current world. Hopefully I'll prove that Clarke's law is not (yet) true.
>
>"Any sufficiently advanced technology is indistinguishable from magic." Arthur C. Clarke

This is the pedantic abstract of *Buzzwords* and it can be listened [here](https://anchor.fm/buzzwords) and, soon, in most of the major podcast sites.

The idea is releasing one or two episodes per month, but, as with everything I start, we'll see how it unfolds.

Some of the terms I plan to talk about:

- Internet
- Cloud
- Processor
- Computer
- Operative system
- Coding
- Artificial intelligence
- Machine learning

Hope I stick with this project!
