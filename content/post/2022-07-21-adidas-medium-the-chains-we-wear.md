---
title: "adidas Engineering Blog post: The chains we wear"
date: 2022-07-21
tags: ["containers", "container-images", "security", "supply-chain"]
---

After a while, I've published my third post in the [adidas Engineering Blog](https://medium.com/adidoescode). The topic
this time is quite disturbing: the dangers of Dockerhub for your containers' supply chain security.

[Check it out here.](https://medium.com/adidoescode/the-chains-we-wear-12dade454925)
