---
title: "Browsing Hygiene: My Holy Trinity Of Browser Plugins"
date: 2021-10-12
tags: ["internet"]
---

A few days ago, a friend asked me about computer security and privacy. He wanted to learn how to be safe
online and be up-to-date with the different kinds of risks that exist on the internet. Also, he doesn't
have a background related to technology.

I must admit that I blanked. That request is very logical and pertinent in our current world, but how
do I answer it? I have spent the best part of the past six years learning about computers and don't
feel even remotely close to provide a comprehensive response. And in terms of security and privacy it is 
not only a matter of technical knowledge, but also about understanding the social aspects of the
dangers and building safety habits. 

Don't get me wrong, anyone can learn about it, but it takes time (less with proper educational materials
from someone more competent than me).

However, the same way you don't need to study medicine to know about basic hygiene that protects you 
from infections, there are some actions that have direct positive impact in your online life without
the need of prior knowledge. That I'm able to answer!

So I ended up sharing with my friend my Holy Trinity of browser plugins. These are three simple
browser extensions that I immediately install in every web browser I use. 
The three of them work with most desktop web browsers such as Firefox, Chrome, Edge and Opera. 
However, at this moment the only way I know to easily use them in Android is installing the
[Firefox app for Android](https://play.google.com/store/apps/details?id=org.mozilla.firefox&hl=en_US&gl=US).

**NOTE:** I also use another plugin called [NoScript](https://noscript.net/), but it is not trivial 
to understand and may be frustrating sometimes (I indeed get frustrated a lot in some webs like 
[Twitch](https://www.twitch.tv/)).

# [uBlock Origin](https://ublockorigin.com/)

Modern webpages are crowded with trackers to show you ads. These little snitches gather information
about what you click, your location, your devices... All of which is used to uniquely identify 
you across the internet and categorize your interests, mostly to sell you stuff (but not only).

The privacy implications of the ad industry are serious enough, but its effects extend to worse performance
of sites and significant increases in the bandwidth used to load them. Both of these issues are specially 
relevant on mobile devices, where internet connection is unreliable and limited.

**uBlock Origin** is the best ad/tracker blocker out there. Not only it is extremely efficient, but it also is a
solid open-source project built on extensive voluntary work, so it doesn't sell companies the right to skip 
the filter, like [AdBlock Plus and others do](https://adblockplus.org/en/about#monetization).

Install it on:

- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
- [Firefox for Android](https://addons.mozilla.org/en-US/android/addon/ublock-origin/)
- [Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)


# [HTTPS Everywhere](https://www.eff.org/https-everywhere)

When you navigate to a webpage, your browser connects to it using either HTTP or HTTPS, the latter being 
like the former but *Secure*. If you use HTTPS, only you and the webpage know what you are sending and 
receiving; otherwise, you are broadcasting openly your communication to the internet, including every
password, credit card and similar that you introduce. Also, you can't know for sure if what you receive
is what was originally intended or someone tampered it in the way.

By now, all webpages should be running exclusively on HTTPS, but, either because of ignorance or incompetence,
we are not quite there yet. That is why **HTTPS Everywhere** shows a big scary warning when you try to navigate
to a site that doesn't accept a secure connection. When this happens it is still possible to accept the risk
and continue, but now you know that you should _**NEVER**_ introduce sensitive information in that site 
(also, beware of what you download from it).

Install it on:

- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
- [Firefox Android](https://addons.mozilla.org/en-US/android/addon/https-everywhere/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=featured)
- [Chrome](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp)


# [Bitwarden](https://bitwarden.com/)

Nowadays most people who use the internet have accounts in several sites, sometimes even dozens. Still,
many don't know or care about the most important rule of safety:

**Use a long, complex and unique password for every account you own.**

This is not even a piece of advice; it is a critical self-preservation measure. When you use a single password
across all your accounts, you are one mistake away from identity theft, with all its nasty consequences.
And this mistake may be that one of the webpages you use suffers a security breach, which is very much out
of your control.

That is why you need to use a password manager. A password manager creates random passwords for each site
and remembers them, so you don't have to. This way, the only password you need to memorize is the one that
opens the manager, which you'll ensure is strong enough for the duty. Take a look at 
[this article](https://www.nist.gov/blogs/taking-measure/easy-ways-build-better-p5w0rd)
about creating a safe and easy to remember passphrase.

There are multiple password managers out there, including the ones integrated in modern web browsers, but I
personally use **Bitwarden**. Its basic plan is free and upgrading to a paid version won't harm your pocket 
either; it is backed on the cloud, but you can make a local backup for redundancy; and it is available both 
as web browser plugin and smartphone app, so you can access your passwords from all your devices.

Install it on:

- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
- [Chrome](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb)
- [Android app](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden&hl=en&gl=US)


# Closing up

It is my humble opinion that using these three amazing plugins will make you a happier, safer human.
Of course, this doesn't protect you from the darkest forces of evil, but the benefits are huge compared
with the minimal effort it takes.

From here, you should spend some time learning about more proactive approaches to security, like being
alert about [phishing](https://en.wikipedia.org/wiki/Phishing) 
and [malicious URLs](https://blog.knowbe4.com/top-12-most-common-rogue-url-tricks). 
I can assure you that at some point you will find this knowledge useful.

Be safe out there!
