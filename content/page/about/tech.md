---
title: About me
subtitle: Your friendly cloud engineer neighbour
comments: false
---

I'm a computer engineer graduated from the University of Zaragoza (Spain) and specialised in cloud and distributed
systems.

During my career I've worked in platform, product and SRE roles in:

- [adidas](https://www.adidas.es/)
- [Scaleway](https://www.scaleway.com/en/)
- [System73](https://www.system73.com/)

About certifications:

- [AWS Certified Solution Architect - Associate](https://www.credly.com/badges/e08b9422-bb44-4e1d-8051-35e1aa74d9b4/public_url)
- [Certified Kubernetes Administrator](https://www.credly.com/badges/a9ec5222-9a98-4711-84b7-c9834b88d607?source=linked_in_profile)
