---
title: Sobre mí
subtitle: Digo cosas, hago movidas y viceversa
comments: false
---

Llevo tres años haciendo lo de la risa en Zaragoza junto a mis compañeros de [Somarda's Comedy](https://somardas.co), con
los que gestiono un micro abierto de comedia y producimos espectáculos todos los meses. 

En este tiempo he tenido la oportunidad de desarrollarme y actuar en Zaragoza, Barcelona, Logroño, Teruel...
¡Y próximamente en tu ciudad! *guiño de teletienda*

Creo que hacer reír a otras personas es el mejor regalo posible, y mi objetivo es dedicar a ello el resto de mi vida.

Por lo pronto, te invito a que te suscribas a mi newsletter [*Gritando al vacío*](https://news.miguescri.com),
en la que hago sátira, narrativa, y además es gratis. Mira tú qué bien.

---

Lo que digo que soy:

- Monologuista
- Improvisador
- Guionista
- Escritor
- Poeta
- Productor

Lo que realmente soy:

- Pesado
- Pedante
- Persona (a ratos) 

---

Mi formación artística y literaria ha sido eminentemente autodidacta y a base de golpes. Y se nota.

Sin embargo, he tenido la fortuna de poder aprender de grandes artistas como:

- Ignasi Taltavull
- Jorge Usón
- Sergio Jabaten
- Miguel Ortega
- Jon Quílez
- Fran Martínez

Y, por supuesto, todos los profes de [La Llama School](https://lallamaschool.com).

¡Que viva la risa!